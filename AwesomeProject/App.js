import React from 'react';
import { Button, View, Text, StyleSheet } from 'react-native';
import { createStackNavigator } from 'react-navigation'; // Version can be specified in package.json

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#FFB6C1' }}>
        <Text>Welcome to Home!!</Text>
        <Text style={styles.paragraph}>
          Daisy Ayesah Aguilar
        </Text>
        <Button
          title="Go to Details"
          onPress={() => this.props.navigation.navigate('Details')}
        />



      </View>
    );
  }
}

class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#87CEEB' }}>
        <Text style={styles.paragraph}>
          This is the Detail Screen
        </Text>

        <Button
          title="Back to Home"
          onPress={() => this.props.navigation.navigate('Home')}
        />
        
      </View>
    );
  }
}




const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
    
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}


const styles = StyleSheet.create({
  
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});